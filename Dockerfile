FROM debian:9 as build
RUN apt update && apt install -y vim wget gcc build-essential make
RUN wget https://www.zlib.net/zlib-1.2.11.tar.gz && tar xvfz zlib-1.2.11.tar.gz -C /usr/lib/
RUN wget https://ftp.pcre.org/pub/pcre/pcre-8.37.tar.gz && tar xvfz pcre-8.37.tar.gz -C /usr/lib/
RUN wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20200102.tar.gz && tar xvfz v2.1-20200102.tar.gz -C /usr/lib/
RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.19.tar.gz && tar xvfz v0.10.19.tar.gz -C /usr/lib/
RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && tar xvfz v0.3.1.tar.gz -C /usr/lib/ 
RUN make install -C /usr/lib/luajit2-2.1-20200102 
RUN wget http://nginx.org/download/nginx-1.19.3.tar.gz && tar xvfz nginx-1.19.3.tar.gz && cd nginx-1.19.3 && export LUAJIT_LIB=/usr/local/lib && export LUAJIT_INC=/usr/local/include/luajit-2.1/  && ./configure --with-zlib=/usr/lib/zlib-1.2.11 --with-pcre=/usr/lib/pcre-8.37 --with-ld-opt="-Wl,-rpath,/usr/local/lib" --add-module=/usr/lib/ngx_devel_kit-0.3.1 --add-module=/usr/lib/lua-nginx-module-0.10.19  && make && make -j2 && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]
